#pragma once

#include "ModuleManager.h"

class DerandomizeISMCsImpl : public IModuleInterface
{
public:
  /** IModuleInterface implementation */
  void StartupModule();
  void ShutdownModule();
};