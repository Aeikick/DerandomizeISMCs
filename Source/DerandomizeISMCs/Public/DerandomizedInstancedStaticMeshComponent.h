#include "CoreMinimal.h"
#include "Engine/Private/InstancedStaticMesh.h"
#include "Components/InstancedStaticMeshComponent.h"


#include "DerandomizedInstancedStaticMeshComponent.generated.h"

#pragma once


UCLASS(ClassGroup = Rendering, meta = (BlueprintSpawnableComponent), Blueprintable)
class ENGINE_API UDerandomizedInstancedStaticMeshComponent : public UInstancedStaticMeshComponent
{
  GENERATED_BODY()

  void BuildRenderData(FStaticMeshInstanceData& OutData, TArray<TRefCountPtr<HHitProxy>>& OutHitProxies);
};
