#include "CoreMinimal.h"
#include "Engine/Private/InstancedStaticMesh.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"


#include "DerandomizedHierarchicalInstancedStaticMeshComponent.generated.h"

#pragma once


UCLASS(ClassGroup = Rendering, meta = (BlueprintSpawnableComponent), Blueprintable)
class ENGINE_API UDerandomizedHierarchicalInstancedStaticMeshComponent : public UHierarchicalInstancedStaticMeshComponent
{
  GENERATED_BODY()

  FPrimitiveSceneProxy* CreateSceneProxy();
  bool BuildTreeIfOutdated(bool Async, bool ForceUpdate);

private:
  void hackIDs();
};
