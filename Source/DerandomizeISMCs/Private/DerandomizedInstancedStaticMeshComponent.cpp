#include "DerandomizedInstancedStaticMeshComponent.h"

void UDerandomizedInstancedStaticMeshComponent::BuildRenderData(FStaticMeshInstanceData& OutData, TArray<TRefCountPtr<HHitProxy>>& OutHitProxies){
  Super::BuildRenderData(OutData,OutHitProxies);
  for(int32 instance = 0; instance < GetNumRenderInstances(); ++instance)
  {
    FMatrix trans;
    OutData.GetInstanceTransform(instance, trans);
    OutData.SetInstance(instance, trans, float(instance));
  }

}