#include "DerandomizedHierarchicalInstancedStaticMeshComponent.h"

void UDerandomizedHierarchicalInstancedStaticMeshComponent::hackIDs(){

  // rewrite the instance randoms ids to instance ids
  for(int32 instance = 0; instance < this->PerInstanceRenderData->InstanceBuffer.GetNumInstances(); ++instance)
  {
    FMatrix trans;
    this->PerInstanceRenderData->InstanceBuffer.GetInstanceData()->GetInstanceTransform(instance, trans);
    this->PerInstanceRenderData->InstanceBuffer.GetInstanceData()->SetInstance(instance, trans, float(instance));
  }
}

FPrimitiveSceneProxy* UDerandomizedHierarchicalInstancedStaticMeshComponent::CreateSceneProxy(){
  auto proxy = Super::CreateSceneProxy();

  if(!this->PerInstanceRenderData.IsValid())
    return proxy;

  hackIDs();

  return proxy;
}

bool UDerandomizedHierarchicalInstancedStaticMeshComponent::BuildTreeIfOutdated(bool Async, bool ForceUpdate){
  bool ret = Super::BuildTreeIfOutdated(Async,ForceUpdate);

  if(!this->PerInstanceRenderData.IsValid())
    return ret;

  hackIDs();

  return ret;
}
