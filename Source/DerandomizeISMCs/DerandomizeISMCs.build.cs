// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DerandomizeISMCs : ModuleRules
{
  public DerandomizeISMCs(ReadOnlyTargetRules Target) : base(Target)
  {
    PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

    PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine" });

    PrivateDependencyModuleNames.AddRange(new string[] { "Engine" });

    PrivateIncludePaths.AddRange(new string[] { "DerandomizeISMCs/Private" });
    PublicIncludePaths.AddRange(new string[] { "DerandomizeISMCs/Public" });


  }
}
